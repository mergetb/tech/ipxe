#!/bin/bash

set -e

BUILDS=("ttyS0" "ttyS1" "ahi")

for i in ${BUILDS[@]}; do
	mkdir -p bin-$i

	make \
	  bin-x86_64-efi/ipxe.efi \
	  bin-x86_64-efi/snponly.efi \
	  -j`nproc` EMBED=sled-$i.ipxe

	EXTRA_CFLAGS=-fno-pie make \
	  bin-x86_64-pcbios/ipxe.pxe \
	  -j`nproc` EMBED=sled-$i.ipxe

	cp bin-x86_64-efi/ipxe.efi bin-$i/
	cp bin-x86_64-efi/snponly.efi bin-$i/
	cp bin-x86_64-pcbios/ipxe.pxe bin-$i/

	make veryclean
done
